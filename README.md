# FTP Client-Server Socket

## Daftar Kelompok
1. Aedhelio Pratama (1301160271)
2. Alfiansyah Nur Abadi (1301164026)
3. Yudha Alsepky Hadi (1301164126)
4. Nurul Izzah (1301164504)

## Description
FTP (File Transfer Protocol) adalah protokol internet yang berjalan dalam satu
lapisan aplikasi yang berfungsi untuk tukar menukar data antara Client dan
Server dalam satu jaringan. Disini kami membuat sebuah layanan untuk tukar
menukar data antara Client dan Server menggunakan Socket sebagai perantara dalam
pertukaran data tersebut, dimana Client dan Server saling terhubung dalam satu
jaringan sehingga memungkinkan untuk melakukan tukar menukar data. Untuk dapat
saling terhubung Client dan Server harus menggunakan koneksi yang sama, kemudian
Client dapat memanggil fungsi Connect agar dapat terhubung ke Server. Setelah
terhubung, Client dapat memilih fitur lainnya yaitu Upload, Download, List, Delete, atau Create Directory.
Dimana fitur Upload dapat digunakan untuk mengirim file dari Client ke Server,
fitur Download dapat digunakan untuk mendownload file yang ada di Server dan
kemudian akan tersimpan di Client, fitur List dapat digunakan untuk menampilkan
list file yang ada di Server. Fitur Delete dapat digunakan untuk menghapus file yang terdapat pada Server. Fitur Create Directory dapat digunakan untuk
membuat direktori baru pada Client.
Jika ingin mengakhiri koneksi ke Server maka Client
dapat memanggil fungsi Quit sehingga koneksi akan berakhir.

## Feature
1. Connect
2. Quit
2. Upload File
2. Download File
3. List File
4. Delete File
5. Create Directory
6. Login
7. Rename File

## Server Details
1. Host : 192.168.1.12 (Tergantung koneksi dari Server)
2. Port : 8080

## Requirements
1. Python 2
2. Socket