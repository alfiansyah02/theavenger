import socket
import sys
import os
import struct
import hashlib
import getpass
from ftplib import FTP

TCP_IP = "192.168.1.12" 
TCP_PORT = 8080 
BUFFER_SIZE = 1024 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


try:
    s.connect((TCP_IP, TCP_PORT))
    print "\nConnection successful"
except:
    print "Connection unsucessful"

def upld(file_name):
    
    print "\nUploading file: {}".format(file_name)
    try:
        
        content = open(file_name, "rb")
    except:
        print "Couldn't open file"
        return
    try:
        
        s.send("UPLD")
    except:
        print "Couldn't make server request"
        return
    try:
        
        s.recv(BUFFER_SIZE)
        
        s.send(struct.pack("h", sys.getsizeof(file_name)))
        s.send(file_name)
        
        s.recv(BUFFER_SIZE)
        s.send(struct.pack("i", os.path.getsize(file_name)))
    except:
        print "Error sending file details"
    try:
        
        l = content.read(BUFFER_SIZE)
        print "\nUploading..."
        while l:
            s.send(l)
            l = content.read(BUFFER_SIZE)
        content.close()
        
        upload_time = struct.unpack("f", s.recv(4))[0]
        upload_size = struct.unpack("i", s.recv(4))[0]
        print "\nFile : {}\nTime : {}s\nSize : {}b".format(file_name, upload_time, upload_size)
    except:
        print "Error sending file"
        return
    return

def dwld(file_name):
    
    print "\nDownloading file: {}".format(file_name)
    try:
        
        s.send("DWLD")
    except:
        print "Couldn't make server request"
        return
    try:
        
        s.recv(BUFFER_SIZE)
        
        s.send(struct.pack("h", sys.getsizeof(file_name)))
        s.send(file_name)
        
        file_size = struct.unpack("i", s.recv(4))[0]
        if file_size == -1:
            
            print "File does not exist"
            return
    except:
        print "Error checking file"
    try:
        
        s.send("1")
        
        output_file = open(file_name, "wb")
        bytes_recieved = 0
        print "\nDownloading..."
        while bytes_recieved < file_size:
            
            l = s.recv(BUFFER_SIZE)
            output_file.write(l)
            bytes_recieved += BUFFER_SIZE
        output_file.close()
        print "\nSuccessfully downloaded {}".format(file_name)
        
        s.send("1")
        
        time_elapsed = struct.unpack("f", s.recv(4))[0]
        print "Time : {}s\nSize : {}b".format(time_elapsed, file_size)
    except:
        print "Error downloading file"
        return
    return

def list_files():
    print("Requesting files...\n")
    try:
        s.send("LIST".encode("ascii"))
    except:
        print("Couldn't make server request")
        return
    try:
        number_of_files = struct.unpack("i", s.recv(4))[0]

        for i in range(int(number_of_files)):
            file_name_size = struct.unpack("i", s.recv(4))[0]
            file_name = s.recv(file_name_size)
            file_size = struct.unpack("i", s.recv(4))[0]

            print("\t{} - {}b".format(file_name, file_size))

            s.send("1".encode("ascii"))

        total_directory_size = struct.unpack("i", s.recv(4))[0]
        print("Total directory size: {}b".format(total_directory_size))
    except:
        print("Couldn't retrieve listing")
        return
    try:
        s.send("1".encode("ascii"))
        return
    except:
        print("Couldn't get final server confirmation")
        return

def delf(file_name):

    print "Deleting file: {}...".format(file_name)
    try:

        s.send("DELF")
        s.recv(BUFFER_SIZE)
    except:
        print "Couldn't connect to server"
        return
    try:

        s.send(struct.pack("h", sys.getsizeof(file_name)))
        s.send(file_name)
    except:
        print "Couldn't send file details"
        return
    try:

        file_exists = struct.unpack("i", s.recv(4))[0]
        if file_exists == -1:
            print "The file does not exist on server"
            return
    except:
        print "Couldn't determine file existance"
        return
    try:

        confirm_delete = raw_input("Are you sure you want to delete {}? (Y/N)\n".format(file_name)).upper()

        while confirm_delete != "Y" and confirm_delete != "N" and confirm_delete != "YES" and confirm_delete != "NO":

            print "Command not recognized"
            confirm_delete = raw_input("Are you sure you want to delete {}? (Y/N)\n".format(file_name)).upper()
    except:
        print "Couldn't confirm deletion status"
        return
    try:

        if confirm_delete == "Y" or confirm_delete == "YES":

            s.send("Y")

            delete_status = struct.unpack("i", s.recv(4))[0]
            if delete_status == 1:
                print "File successfully deleted"
                return
            else:

                print "File failed to delete"
                return
        else:
            s.send("N")
            print "Delete abandoned"
            return
    except:
        print "Couldn't delete file"
        return

def login():
    login = raw_input("Login? (Y/N) : ")
    if login == "y" or login == "Y":
        s.send("LOGIN")
        user = raw_input("Username: ")
        mu = hashlib.md5()
        mu.update(user)
        s.send("USER " + mu.hexdigest())
        s.recv(BUFFER_SIZE)

        continueword = getpass.getpass("Password: ")
        mp = hashlib.md5()
        mp.update(continueword)
        s.send("PASS " + mp.hexdigest())
        s.recv(BUFFER_SIZE)
    else:
        s.send("QUIT")
        s.close()
        return False
    return

def createDirect(ftp, dirpath):
    dirpath = dirpath.replace('\\', '/')
    temp = dirpath.split('/')
    dirs = []

    for _ in temp:
        if len(dirs) == 0:
            dirs.append(_)
            continue

        dirs.append(dirs[-1] + '/' + _)
    for _ in dirs:
        try:
            ftp.mkd(_)
        except error_perm as e:
            e_str = str(e)
            if '550' in e_str and 'File exists' in e_str:
                continue
            
def rnem():
     print "Rename file: {}...".format(file_name)
     try:

        s.send("RENM")
        s.recv(BUFFER_SIZE)
    except:
        print ("Couldn't connect to server")
        return
    try:

        s.send(struct.pack("h", sys.getsizeof(file_name)))
        s.send(file_name)
    except:
        print ("Couldn't send file details")
        return
    try:

        file_exists = struct.unpack("i", s.recv(4))[0]
        if file_exists == -1:
            print ("File tidak ada diserver")
            return
    except:
        print ("Couldn't determine file existance")
        return
    try:

        confirm_rename = raw_input("Are you sure you want to rename {}? (Y/N)\n".format(file_name)).upper()

        while confirm_rename != "Y" and confirm_rename != "N" and confirm_rename != "YES" and confirm_rename != "NO":

            print ("Command tidak dikenal")
            confirm_rename = raw_input("Are you sure you want to rename {}? (Y/N)\n".format(file_name)).upper()
    except:
        print("Couldn't confirm deletion status")
        return
    try:

        if confirm_rename == "Y" or confirm_delete == "YES":

            s.send("Y")

            rename_status = struct.unpack("i", s.recv(4))[0])
            if delete_status == 1:
                print ("File berhasil rename")
                return
            else:

                print ("File gagal rename")
                return
        else:
            s.send("N")
            print ("Rename Gagal")
            return
    except:
        print ("Couldn't rename file")
        return

def quit():
    s.send("QUIT")
    
    s.recv(BUFFER_SIZE)
    s.close()
    print "Connection ended"
    return

print "\nFTP CLIENT\n\nUPLD file : Upload file\nDWLD file : Download file\nLIST      : List files\nDELF file : Delete file\nRNEM file : Rename fileQUIT      : Exit"

while True:
    
    prompt = raw_input("\nCommand : ")
    if prompt[:4].upper() == "CONN":
        conn()
    elif prompt[:4].upper() == "CRT":
        upld(prompt[5:])
    elif prompt[:4].upper() == "UPLD":
        upld(prompt[5:])
    elif prompt[:4].upper() == "DWLD":
        dwld(prompt[5:])
    elif prompt[:4].upper() == "LIST":
        list_files()
    elif prompt[:4].upper() == "DELF":
        delf(prompt[5:])
    elif prompt[:4].upper() == "RNEM":
        rnm()(prompt[5:])
    elif prompt[:4].upper() == "QUIT":
        quit()
        break
    else:
        print "Command not recognized"

