import socket
import sys
import time
import os
import struct
import json
import hashlib
import getpass
from ftplib import FTP

print "\nFTP SERVER"

TCP_IP = "192.168.1.12"
TCP_PORT = 8080 
BUFFER_SIZE = 1024 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(3)
conn, addr = s.accept()

print "\nConnected to : {}".format(addr)

def upld():
    
    conn.send("1")
    
    file_name_size = struct.unpack("h", conn.recv(2))[0]
    file_name = conn.recv(file_name_size)
    
    conn.send("1")
    
    file_size = struct.unpack("i", conn.recv(4))[0]
    
    start_time = time.time()
    output_file = open(file_name, "wb")
    
    bytes_recieved = 0
    print "\nRecieving..."
    while bytes_recieved < file_size:
        l = conn.recv(BUFFER_SIZE)
        output_file.write(l)
        bytes_recieved += BUFFER_SIZE
    output_file.close()
    print "\nRecieved file: {}".format(file_name)
    
    conn.send(struct.pack("f", time.time() - start_time))
    conn.send(struct.pack("i", file_size))
    return

def dwld():
    conn.send("1")
    file_name_length = struct.unpack("h", conn.recv(2))[0]
    
    file_name = conn.recv(file_name_length)
    print file_name
    if os.path.isfile(file_name):
        
        conn.send(struct.pack("i", os.path.getsize(file_name)))
    else:
        
        print "File name not valid"
        conn.send(struct.pack("i", -1))
        return
    
    conn.recv(BUFFER_SIZE)
    
    start_time = time.time()
    print "\nSending..."
    content = open(file_name, "rb")
    
    l = content.read(BUFFER_SIZE)
    while l:
        conn.send(l)
        l = content.read(BUFFER_SIZE)
    content.close()
    
    conn.recv(BUFFER_SIZE)
    conn.send(struct.pack("f", time.time() - start_time))
    return

def list_files():
    print("Listing files...")
    listing = os.listdir(os.getcwd())

    total_directory_size = 0
    data2Send = []
    for i in listing:
        data2Send.append({"file": i, "size": os.path.getsize(i)})

        print(i)        

    dat = json.dumps(data2Send)
    conn.send(struct.pack("i", len(dat)))
    conn.send(dat.encode("ascii"))
    print("Successfully sent file listing")
    conn.recv(BUFFER_SIZE)
    return

def delf():

    conn.send("1")

    file_name_length = struct.unpack("h", conn.recv(2))[0]
    file_name = conn.recv(file_name_length)

    if os.path.isfile(file_name):
        conn.send(struct.pack("i", 1))
    else:

        conn.send(struct.pack("i", -1))

    confirm_delete = conn.recv(BUFFER_SIZE)
    if confirm_delete == "Y":
        try:

            os.remove(file_name)
            conn.send(struct.pack("i", 1))
        except:

            print "Failed to delete {}".format(file_name)
            conn.send(struct.pack("i", -1))
    else:

        print "Delete abandoned"
        return

def login():
	if conn.recv(BUFFER_SIZE) == "LOGIN":
		User = conn.recv(37)
		Username = User.split(" ")[1]
		if UserConfig.has_key(Username):
			Pass = conn.recv(37)
			Password = Pass.split(" ")[1]
			if Password == UserConfig[Username]:
				conn.send("Login...")
				return
			else:
				return
		else:
			return
	else:
		return
	return

def createDirect(ftp, dirpath):
    dirpath = dirpath.replace('\\', '/')
    temp = dirpath.split('/')
    dirs = []

    for _ in temp:
        if len(dirs) == 0:
            dirs.append(_)
            continue

        dirs.append(dirs[-1] + '/' + _)
    for _ in dirs:
        try:
            ftp.mkd(_)
        except error_perm as e:
            e_str = str(e)
            if '550' in e_str and 'File exists' in e_str:
                continue

def rnem():
    conn.send("1")

    file_name_length = struct.unpack("h", conn.recv(2))[0]
    file_name = conn.recv(file_name_length)
    if os.path.isfile(file_name):
        conn.send(struct.pack("i", 1))
    else
        conn.send(struct.pack("i", -1))

    confirm_rename = conn.recv(BUFFER_SIZE)
    if confirm_rename == "Y":
        try:
            os.rename(file_name)
            conn.send(struct.pack("i", 1))
        except:
            print("Gagal Rename")
            return

def quit():
    
    conn.send("1")
    
    conn.close()
    s.close()
    os.execl(sys.executable, sys.executable, *sys.argv)

while True:
    
    data = conn.recv(BUFFER_SIZE)
    print "\nRecieved instruction: {}".format(data)
    
    if data.decode("ascii") == "UPLD":
        upld()
    elif data.decode("ascii") == "DWLD":
        dwld()
    elif data.decode("ascii") == "LIST":
        list_files()
    elif data.decode("ascii") == "DELF":
        delf()
    elif data.decode("ascii") == "RNEM":
        renm()
    elif data.decode("ascii") == "QUIT":
        quit()
        elif data.decode("ascii") == "CTR":
        createDirect(ftp, dirpath)
    
    data = None